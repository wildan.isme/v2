import React from 'react'
import { Link } from 'gatsby'
import { Box, Heading, Button } from 'theme-ui'
import Section from '@components/Section'

/**
 * Shadow me to add your own content
 */

const styles = {
  heading: {
    mb: 4,
    span: {
      color: `omegaDark`
    }
  },
  running: {
    color: `omegaDark`,
    fontWeight: `body`,
    width: `5/6`
  }
}

export default () => (
  <>
    <Section>
      <Heading variant='h1' sx={styles.heading}>
        Hai! <span>Nama Saya Wildan</span> 
      </Heading>
      <Heading variant='h3' sx={styles.running}>
        Saya seorang <strong><s>programmer</s></strong> biasa - biasa saja. Yang hanya melakukan Coding dengan banyak melihat Dokumentasi.
      </Heading>
    </Section>
    <Box variant='buttons.group' sx={styles.buttons}>
      {/* <Button as={Link} to='/'>
        Start Learning
      </Button>
      <Button variant='white' as={Link} to='/authors'>
        View Tutors
      </Button> */}
    </Box>
  </>
)
