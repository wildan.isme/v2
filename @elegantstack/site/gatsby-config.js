module.exports = 
{
  flags: 
  {
    DEV_SSR: false
  },
  plugins: 
  [
    // {
    //   resolve: 'gatsby-plugin-netlify-cms',
    //   options: {}
    // },
    {
      resolve: "gatsby-plugin-manifest",
      options: 
      {
        name: 'Wildanisme',
        short_name: 'Catatan Kecil',
        start_url: '/',
        background_color: '#ffffff',
        theme_color: '#5a67d8',
        display: 'minimal-ui',
        icon: 'content/assets/favicon.png',
      }
    },
    {
      resolve: '@elegantstack/gatsby-theme-flexiblog-education',
      options: 
      {
        // Theme options goes here
        sources:
        {
          local: true
        }
      }
    },
  ],
  // Customize your site metadata:
  siteMetadata: 
  {
    //General Site Metadata
    title: 'Wildanisme',
    name: 'Wildanisme',
    description: 'Sebuah Catatan Kecil.',
    address: 'Bogor, ID',
    email: 'wildantaufiq.abdulaziz@gmail.com',
    phone: '+6282124088548',

    //Site Social Media Links
    social: [
      {
        name: 'Github',
        url: 'https://github.com/wildanisme'
      },
      {
        name: 'Twitter',
        url: 'https://twitter.com/HelloSystemD'
      },
      {
        name: 'Instagram',
        url: 'https://instagram.com/wildan_isme'
      }
    ],

    //Header Menu Items
    headerMenu: [
      {
        name: 'Home',
        slug: '/'
      },
      {
        name: 'About Me',
        slug: '/authors'
      },
      {
        name: 'Contact',
        slug: '/contact'
      }
    ],

    //Footer Menu Items (2 Sets)
    footerMenu: [
      {
        title: 'Quick Links',
        items: [
          {
            name: 'Advertise with us',
            slug: '/contact'
          },
          {
            name: 'About',
            slug: '/about'
          },
          {
            name: 'Contact',
            slug: '/contact'
          }
        ]
      },
      {
        title: 'Legal Stuff',
        items: [
          {
            name: 'Privacy Notice',
            slug: '/'
          },
          {
            name: 'Cookie Policy',
            slug: '/'
          },
          {
            name: 'Terms Of Use',
            slug: '/'
          }
        ]
      }
    ]
  }
}
